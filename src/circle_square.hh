/* Copyright 2008 Jason Kim Chong Polak

Please respect the following:

This file is part of bioCity. bioCity is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _CIRCLE_SQUARE_HH_
#define _CIRCLE_SQUARE_HH
#include <gsl/gsl_rng.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_randist.h>

double area_inside ( const double square_x, const double square_y, const double circ_x, const double circ_y, const double circ_r, int resolution );

void unif_circle(double *x, double *y, const gsl_rng *r);
#endif
