/*
 * Definition of base package of scenarios
 */

#include "FuncSelect.hh"

void World::scen_single_pop_init( )
{
	using single_pop::movement_circle;

	using single_pop::reproduction;
	using single_pop::range;
	
	using single_pop::dist_life;
	using single_pop::life_a;
	using single_pop::life_b;
	using single_pop::life_c;
	
	using single_pop::repr_inhibit;

	ConfReader conf( "scenarios/scen_single_pop.conf" );
	if ( conf.success() )
	{
		conf.field_double( "movement_circle", &movement_circle);

		conf.field_double( "reproduction", &reproduction );
		conf.field_double( "range", &range);
		conf.field_string( "dist_life", &dist_life);
		conf.field_double( "life_a", &life_a);
		conf.field_double( "life_b", &life_b);
		conf.field_double( "life_c", &life_c);
		
		conf.field_int( "repr_inhibit", &repr_inhibit );
	}

	cout<<"#dist_life: "<< dist_life<<endl;
}

void World::scen_single_pop( )
{
	using namespace single_pop;

	double currx,curry,rx,ry;
	calc_proximity( range );
	list<Organism*>::iterator i1 = citizens.begin();

	int type1,type2;
	for ( int i = 0; i < population; i++ )
	{
		unif_circle( &rx, &ry, rgen);
		rx*=movement_circle;
		ry*=movement_circle;

		currx = (*i1)->get_x();
		curry = (*i1)->get_y();
		if ( currx + rx > dim_x || currx + rx < 0 ) { rx *= -1; }
		if ( curry + ry > dim_y || curry + ry < 0 )	{ ry *= -1;	}

		(*i1)->change_pos( currx + rx, curry + ry);

		i1++;
	}

	//Begin: Reproduction Loop
	i1 = citizens.begin();
	double will_reproduce;
	int curr_pop = population;
	for ( int i = 0 ; i < curr_pop; i++)
	{
		int currtype = (*i1)->get_type();
		will_reproduce = gsl_rng_uniform( rgen );
		double test_cond = reproduction/
			(1 + repr_inhibit*(*i1)->get_proximity() );

		if (will_reproduce < test_cond )
		{
			(*i1)->set_offspring( 1 );
		}	
		i1++;
	}
	//END: Reproduction Loop

	// See if any citizens are on the verge
	// of dying.
	i1 = citizens.begin( );
	double death = 0;
	double death_prob = 0;
	double death_params[4];
	int death_params_len = 4;
	while ( i1 != citizens.end() )
	{
		if ( (*i1)->get_state() == 1 )
		{
			death_params[0] = (*i1)->get_age();
			death_params[1] = life_a;
			death_params[2] = life_b;
			death_params[3] = life_c;
			FuncSelect death_dist( dist_life, death_params, death_params_len, NULL );
			death = death_dist.evaluate();
			death_prob = gsl_rng_uniform( rgen );
			if (death_prob < death )
			{
				(*i1)->set_state( 0 );
			}
		}
		i1++;
	}
}
