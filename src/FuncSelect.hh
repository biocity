/* Copyright 2008 Jason Kim Chong Polak

Please respect the following:

This file is part of bioCity. bioCity is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * FuncSelect: Takes double parameters and
 * returns a double, by using a selected
 * function.
 */
#ifndef _FUNCSELECT_HH_
#define _FUNCSELECT_HH_
#include <string>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_cdf.h>

using std::string;

class FuncSelect
{
	string func_name;
	double *params;
	int params_len;
	gsl_rng *rgen;
	
	public:
	
	FuncSelect( );
	FuncSelect( 
			string _func_name,
			double *_params,
			int _params_len,
			gsl_rng *_r
			);
	~FuncSelect( );
	double evaluate();

};

#endif
