/* Copyright 2008 Jason Kim Chong Polak

Please respect the following:

This file is part of bioCity. bioCity is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _WORLD_HH_
#define _WORLD_HH_

#include "Organism.hh"
#include <list>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_cdf.h>
using std::list;

class World
{
	list<Organism*> citizens;
	int population;
	int p_type0;
	int p_type1;

	double dim_x;
	double dim_y;
	gsl_rng *rgen;
	const gsl_rng_type *T;
	//Scenario Variables:
	public:

	World( int _psize, double _dx, double _dy );
	~World( );
	void update();
	//Operating on the list of organisms
	void move_next( );
	void calc_proximity( double _range );
	// void determine_proximity( double _range );

	//Scenarios
	void scen_single_pop_init( );
	void scen_single_pop( );
	
	//status checking functions
	void print_graph(int);
	void print_stats();
	void complete_stats();
	void print_proximity();
};
#endif
