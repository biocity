#include <cmath>
#include <iostream>
#include "Organism.hh"
using namespace std;

Organism::Organism( int _tp, int _st, double _px, double _py )
{
	ident = id;
	id++;
	
	type = _tp;
	state = _st;
	pos_x = _px;
	pos_y = _py;
	next_x = pos_x;
	next_y = pos_y;
	food = 0;
	age = 0;
	proximity = 0;
	next_offspring = 0;
}

int Organism::identify()
{
	return ident;
}

void Organism::next_state( )
{
	age++;
	pos_x = next_x;
	pos_y = next_y;
	proximity = 0;
}

//sensory functions
double Organism::distance_to( const Organism _o )
{
	return( sqrt( 
				(pos_x - _o.pos_x)*(pos_x - _o.pos_x) +
				(pos_y-_o.pos_y)*(pos_y - _o.pos_y) 
				) 
			);
}

int Organism::get_proximity( )
{
	return proximity;
}

void Organism::set_proximity( int _prox )
{
	proximity = _prox;
}

void Organism::add_proximity( int _prox )
{
	proximity+= _prox;
}

void Organism::set_offspring( int _offspring )
{
	next_offspring = _offspring;
}

int Organism::get_offspring( )
{
	return next_offspring;
}

double Organism::get_x()
{
	return( pos_x );
}

double Organism::get_y()
{
	return( pos_y );
}

int Organism::get_type()
{
	return( type );
}

int Organism::get_state()
{
	return state;
}

void Organism::lifeup()
{
	age++;
}

int Organism::get_age()
{
	return age;
}

int Organism::set_state( int _st )
{
	state = _st;
	return state;
}

void Organism::change_pos( double _nx, double _ny )
{
	next_x = _nx;
	next_y = _ny;
}

void Organism::eat_food()
{
	food++;
}

void Organism::use_food()
{
	if (food > 0)
	{
		food--;
	}
}

int Organism::get_food()
{
	return food;
}

void Organism::add_pos( double _nx, double _ny )
{
	pos_x += _nx;
	pos_y += _ny;
}

void Organism::info( )
{
	cout<<"(x,y) = ("<<pos_x<<" , "<<pos_y<<") , state = "<<state<<endl;
}
