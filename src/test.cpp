#include <iostream>
#include <string>
#include "ConfReader.hh"
#include "FuncSelect.hh"
#include <gsl/gsl_rng.h>
#include <gsl/gsl_cdf.h>
#include "World.hh"
#include <iomanip>
using namespace std;

int ConfReaderTest1();
int WorldProxTest();
int FuncSelectTest();
int NoFuncSelectTest(bool);
int GenRanTest();

int main()
{
	int tests = 0;
	int tests_complete = 0;
	gsl_rng *r;


	//ConfReaderTest1();
	//WorldProxTest();
	
	//FuncSelectTest();
	GenRanTest();

	return 0;
}

int GenRanTest()
{
	gsl_rng *r;
	const gsl_rng_type *T = gsl_rng_default;
	r = gsl_rng_alloc(T);

	for (int i = 0; i < 200000; i++)
	{
		cout<<gsl_rng_uniform( r )<<endl;
	}

	return 0;
}

int FuncSelectTest()
{
	double myparams[2];
	myparams[0] = 1;
	myparams[1] = 2;
	int len = 2;

	FuncSelect my_func( "gsl_cdf_tdist_P", myparams, len, NULL);
	double result;
	result = my_func.evaluate();
	cout<<"Returned: "<<setprecision(16)<<result<<endl;
}

int NoFuncSelectTest(bool fsOn = true)
{
	double myparams[2];
	myparams[0] = 0.5;
	myparams[1] = 1;
	int len = 2;
	double sum = 0;
	double temp;
	double j;
	FuncSelect my_func( "gsl_cdf_gaussian_P", myparams, len, NULL);

	for (int i = 0; i < 100000; i++)
	{
		j = (double) i;
		if ( fsOn )
		{
			temp = my_func.evaluate();
		}
		else
		{
			temp = gsl_cdf_gaussian_P( 0.5, myparams[1]);
		}
		sum+=temp;
	}

	cout<<sum<<endl;
}

int WorldProxTest()
{
	World testW( 6, 20, 20);
	testW.calc_proximity( 10 );
	testW.print_proximity();
}

int ConfReaderTest1()
{
	cout<<"ConfReaderTest1"<<endl;

	int success = 0;
	int time=0;
	string scen = "sdsd";
	ConfReader m( "bioc.conf" );
	ConfReader n( "nonexistant.conf" );
	cout<<m.success()<<"\t true"<<endl;
	cout<<n.success()<<"\t false"<<endl;
	m.field_int( "time", &time );
	cout<<time<<endl;
	m.field_string( "scenario", &scen);
	cout<<scen<<endl;
//	cout<<m.get_field_string("scenariof")<<";"<<endl;
}
