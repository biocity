#!/bin/sh
i=1
while [ $i -le 50 ]
do
	dh=`date +%k`
	dm=`date +%M`
	ds=`date +%S`
	dns=`date +%N`
	n=`echo "$ds*10000+$dm*100+$ds+$dns" | bc`
	`GSL_RNG_SEED=$n ./bioc >> dat/data-$i.txt`
	i=$(($i+1))
done
