/* Copyright 2008 Jason Kim Chong Polak

Please respect the following:

This file is part of bioCity. bioCity is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "circle_square.hh"
#include <iostream>
#include <cmath>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_cdf.h>

using namespace std;


double area_inside ( const double square_x, const double square_y, const double circ_x, const double circ_y, const double circ_r, int resolution  = 1000)
{
	double result;
	gsl_rng *cgen;
	const gsl_rng_type *T1;

	if (resolution < 0)
	{
		resolution = 1000;
	}
	gsl_rng_env_setup();
	T1 = gsl_rng_default;
	cgen = gsl_rng_alloc(T1);
	
	double px,py;
	bool inside = false;

	int count_inside = 0;
	for (int i = 0; i < resolution; i++)
	{
		while (!inside)
		{
			px = ( gsl_rng_uniform( cgen ) - 1.0/2 )*2;
			py = ( gsl_rng_uniform( cgen ) - 1.0/2 )*2;
			if ( px*px + py*py <= 1 ) { inside = true; }
		}
		inside = false;
		px = px*circ_r + circ_x;
		py = py*circ_r + circ_y;

		//cout<<px<<"\t"<<py<<endl;

		if (px >= 0 && px <= square_x)
		{
			if (py >=0 && py <= square_y)
			{
				count_inside++;
			}
		}
	}

	result = M_PI*circ_r*circ_r*(static_cast<double>(count_inside)/static_cast<double>(resolution));
	cout<<"-"<<count_inside<<endl;
	if (cgen == NULL) { gsl_rng_free( cgen ); }
	return result;
}

void unif_circle(double *x, double *y, const gsl_rng *r)
{
	double t_x,t_y;
	bool inside = false;
	while (!inside)
	{
		t_x = ( gsl_rng_uniform_pos( r ) - 1.0/2 )*2;
		t_y = ( gsl_rng_uniform_pos( r ) - 1.0/2 )*2;
		if (t_x*t_x + t_y*t_y <=1) { inside = true; }
	}
	*x = t_x;
	*y = t_y;
}

