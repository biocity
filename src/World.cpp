/* Copyright 2008 Jason Kim Chong Polak

Please respect the following:

This file is part of bioCity. bioCity is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "World.hh"
#include "Organism.hh"
#include "scenarios.hh"

#include <g2.h>
#include <g2_X11.h>

#include <list>
#include <iostream>
#include <cmath>
#include "circle_square.hh"
#include "ConfReader.hh"
using std::cout;
using std::endl;
using std::cerr;


//Scenario Defs
#include "World_scenarios_base.cpp"
// "Be who you are and say what you feel
// because those who mind don't matter and
// those who matter don't mind." -Seuss

// Set the organism id numbers starting at 1.
int Organism::id = 1;

World::World( int _psize, double _dx, double _dy )
{
	gsl_rng_env_setup();
	T = gsl_rng_default;
	rgen = gsl_rng_alloc(T);

	if (_dx <= 0 || _dy <= 0) 
	{
		cerr<<"Distances must be positive."<<endl;
	}
	dim_x = _dx;
	dim_y = _dy;
	if (_psize < 0 )
	{
		cerr<<"Number of citizens cannot be negative."<<endl;
	}	
	// setting up the population here
	population = _psize;
	p_type0 = 0;
	p_type1 = 0;

	int org_type;
	double initx;
	double inity;

	for ( int i = 0; i < population; i++)
	{
		org_type = (int) 0.0*floor( sqrt((gsl_rng_uniform( rgen )*2)) );
		if (org_type == 0) { p_type0++; }
		if (org_type == 1) { p_type1++; }
		initx = gsl_rng_uniform( rgen )*dim_x;
		inity = gsl_rng_uniform( rgen )*dim_y;
		Organism *temp = new Organism( org_type, 1, initx ,inity );
		citizens.push_front( temp );
	}
}	

World::~World( )
{	
	list<Organism*>::iterator i1 = citizens.begin();
    for (int i = 0; i < population; i++)
    {
    	if ((*i1)!=NULL) { delete (*i1); }
    	i1++;
    }

	if (rgen != NULL) { gsl_rng_free( rgen ); }
}

void World::move_next( )
{
	int i = 0;
	double currx,curry,rx,ry;
	list<Organism*>::iterator i1 = citizens.begin();
	while (i1 != citizens.end() )
	{	
		if ( (*i1)->get_state( ) == 0 )
		{
			if ( (*i1)->get_type() == 0)
			{
				p_type0--;
			}
			i1 = citizens.erase(i1);
			population--;
		}
		else
		{
			(*i1)->lifeup();
			if (i < population && (*i1)->get_offspring() > 0)
			{
				rx = gsl_rng_uniform( rgen ) - 1.0/2;
				ry = gsl_rng_uniform( rgen ) - 1.0/2;
				currx = (*i1)->get_x();
				curry = (*i1)->get_y();
				if ( currx + rx > dim_x || currx + rx < 0 ) { rx *= -1;	}
				if ( curry + ry > dim_y || curry + ry < 0 ) { ry *= -1; }
				currx += rx;
				curry += ry;
				p_type0++;

				citizens.push_front( new Organism( 0, 1, currx, curry ) );
				population++;
			}
			(*i1)->set_offspring( 0 );
			(*i1)->next_state( );
			i1++;
		}
		i++;
	}
}

void World::calc_proximity( double _range )
{
	list<Organism*>::iterator i1 = citizens.begin( );
	while (i1 != citizens.end())
	{
		(*i1)->set_proximity(0);
		i1++;
	}

	i1 = citizens.begin();
	list<Organism*>::iterator i2 = citizens.begin( );
	i2++;
	while (i1 != citizens.end())
	{
		while (i2 != citizens.end() )
		{
			if ( (*i1)->distance_to( **i2 ) < _range )
			{
				(*i1)->add_proximity(1);
				(*i2)->add_proximity(1);
			}
			i2++;
		}
		i1++;
		i2=i1;
		i2++;
	}
}

/*
void World::update( )
{
	double reproduction = 0.05;
	double kill_rate = 0.3;
	double average_lifetime = 100;

	double range = 50;
	double kill_range = 5;
	double kill_prob = 0;

	double currx,curry,rx,ry;

	// Movement and Predation Loop
	list<Organism*>::iterator i1 = citizens.begin();
	list<Organism*>::iterator i2 = citizens.begin();
	i2++;
	int type1,type2;
	for ( int i = 0; i < population; i++ )
	{
		(*i1)->lifeup();	//advance in age after each update

		unif_circle( &rx, &ry, rgen);
		rx*=2;
		ry*=2;

		currx = (*i1)->get_x();
		curry = (*i1)->get_y();
		if ( currx + rx > dim_x || currx + rx < 0 ) { rx *= -1; }
		if ( curry + ry > dim_y || curry + ry < 0 )	{ ry *= -1;	}

		(*i1)->add_pos( rx, ry );
		for ( int j = i + 1; j < population; j++)
		{
			if ( (*i1)->distance_to( **i2 ) < range )
			{
				int i1p = (*i1)->get_proximity();
				int i2p = (*i2)->get_proximity();
				(*i1)->set_proximity( i1p + 1 );
				(*i2)->set_proximity( i2p + 1 );
			}

			//The predator-prey interaction rules			
			double hunger = 0;
			bool canEat = (*i1)->get_state() == 1;
			canEat = canEat && (*i2)->get_state() == 1;
			type1 = (*i1)->get_type();
			type2 = (*i2)->get_type();
			bool diff =  type1 != type2;
			canEat = canEat && diff;

			if ( (*i1)->distance_to( **i2 ) < kill_range && canEat )
			{
				if (type1 == 1)
				{
					hunger = (*i1)->get_food();
				}
				else
				{
					hunger = (*i2)->get_food();
				}

				kill_prob = gsl_rng_uniform( rgen );
				if (kill_prob < kill_rate*(1-hunger))
				{
					if (type1 == 0)
					{
						(*i1)->set_state( 0 ); //mark for death
						(*i2)->eat_food();
					}
					else
					{
						(*i2)->set_state( 0 ); // " "
						(*i1)->eat_food();
					}
				}
			}
			//End Predator-prey interactions
			
			i2++;
		}
		i1++;
		i2=i1; //Set i2 as the new i1
		i2++; //Then increment i2
	}
	

	//Begin: Reproduction Loop
	i1 = citizens.begin();
	double will_reproduce;
	int cutoff = 20;
	int curr_pop = population;
	for ( int i = 0 ; i < curr_pop; i++)
	{
		int currtype = (*i1)->get_type();
		int type_dep = 1;
		int loc_dep = 1;
		
		//If the predator has no food, it can't reproduce
		if (currtype == 1) { type_dep *= (*i1)->get_food();	}
		
		// If there are more than cutoff organisms in the
		// given range then the organism cannot reproduce
		// if (proximity[i] >= cutoff) {loc_dep = 0; }
		
		will_reproduce = gsl_rng_uniform( rgen );
		double test_cond = reproduction/(1 + (*i1)->get_proximity() );
		(*i1)->set_proximity( 0 );

		if (will_reproduce < test_cond*type_dep*loc_dep )
		{
			rx = gsl_rng_uniform( rgen ) - 1.0/2;
			ry = gsl_rng_uniform( rgen ) - 1.0/2;
			currx = (*i1)->get_x();
			curry = (*i1)->get_y();
			if ( currx + rx > dim_x || currx + rx < 0 ) { rx *= -1;	}
			if ( curry > dim_y || curry + ry < 0 ) { ry *= -1; }
			currx += rx;
			curry += ry;

			if (currtype == 0) { p_type0++; }
			if (currtype == 1) { p_type1++;	}

			Organism *temp = new Organism( currtype, 1, currx, curry );
			citizens.push_front( temp );
			population++;
		}
		i1++;
	}
	//END: Reproduction Loop

	// START: Grim Reaper
	// Removes all organisms that have been eaten,
	// and kills any organism that has died of
	// natural causes
	i1 = citizens.begin( );
	double death = 0;
	double death_prob = 0;
	while ( i1 != citizens.end() )
	{
		if ( (*i1)->get_state() == 1 )
		{
			death = gsl_cdf_gaussian_P( (*i1)->get_age()-average_lifetime ,3.0);
			death_prob = gsl_rng_uniform( rgen );
			if (death_prob < death)
			{
				(*i1)->set_state( 0 );
			}
		}
		if ( (*i1)->get_state() == 0 )
		{
			switch ( (*i1)->get_type() )
			{
				case 0:
					p_type0--;
					break;
				case 1:
					p_type1--;
					break;
				default:
					cout<<"error"<<endl;
					break;
			}
			i1 = citizens.erase( i1 ); //increments pointer too!
			population--;
		}
		else { i1++; }
	}
	//END: Grim Reaper Loop

}
*/

void World::print_graph( int id )
{
	g2_pen( id, 0);
	g2_filled_rectangle( id, 0, 0, dim_x, dim_y );
	g2_pen( id, 1 );
	list<Organism*>::iterator i1 = citizens.begin();
	for (int i = 0; i < population; i++)
	{
		if ( (*i1)->get_type() == 0 )
		{
			g2_pen( id, 3 );
		}
		else if ( (*i1)->get_type() == 1)
		{
			g2_pen( id, 19 );
		}
		else
		{
			//do nothing
		}
		g2_filled_circle( id, (*i1)->get_x(), (*i1)->get_y(), 4 );
		i1++;
	}
}

void World::print_stats( )
{
	cout<<population<<"\t"<<p_type0<<"\t"<<p_type1<<endl;
}

void World::complete_stats( )
{
	double curr_x,curr_y;
	list<Organism*>::iterator i1 = citizens.begin();
	for (int i = 0; i < population; i++)
	{
		curr_x = (*i1)->get_x();
		curr_y = (*i1)->get_y();
		if (curr_x < 0 || curr_x > dim_x || curr_y < 0 || curr_y > dim_y)
		{
			cout<<"out of bounds error..."<<endl;
		}
		i1++;
	}
}

void World::print_proximity()
{
	list<Organism*>::iterator i1 = citizens.begin();
	cout<<"PosX\t PosY\t Prox"<<endl;
	double x,y;
	int p;
	while (i1 != citizens.end())
	{
		x = (*i1)->get_x();
		y = (*i1)->get_y();
		p = (*i1)->get_proximity();
		cout<<x<<"\t"<<y<<"\t"<<p<<endl;
		i1++;
	}
}
