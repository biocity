#include "ConfReader.hh"

ConfReader::ConfReader( string _filename )
{
	filename = _filename;
	ifstream conf( filename.c_str(), ios::out );
	read_success = conf.is_open();

	fields[0] = "default";
	values[0] = "-1";

	string curr_line;
	string curr_field;
	string curr_value;

	int data_begin;
	int i = 1;

	if (read_success)
	{
		while ( !conf.eof() )
		{
			getline( conf, curr_line );
			if ( curr_line.substr(0,1) != "#" )
			{
				data_begin = curr_line.find_first_of(":");
				if (data_begin != -1)
				{
					curr_field = curr_line.substr(0,data_begin);
					curr_value = curr_line.substr(data_begin + 1,
							curr_line.length() - 1);
					fields[i] = curr_field;
					values[i] = curr_value;
					if (i < 16) { i++; }
				}
			}
		}
	}
}

bool ConfReader::field_int( string _field, int *field)
{
	stringstream value;
	int result;
	bool found = false;
	int i = 1;

	while( i < 16 && !found )
	{
		if (fields[i]==_field)
		{
			value.str( values[i] );
			found = true;
		}
		i++;
	}
	if (!found)
	{
		return false;
	}
	value>>result;
	*field = result;
	return found;
}

bool ConfReader::field_double( string _field, double *field)
{
	stringstream value;
	double result;
	bool found = false;
	int i = 1;

	while( i < 16 && !found )
	{
		if (fields[i]==_field)
		{
			value.str( values[i] );
			found = true;
		}
		i++;
	}
	if (!found)
	{
		return false;
	}
	value>>result;
	*field = result;
	return found;
}

bool ConfReader::field_string( string _field, string *field)
{
	stringstream value;
	string result;
	bool found = false;
	int i = 1;

	while( i < 16 && !found )
	{
		if (fields[i]==_field)
		{
			value.str( values[i] );
			found = true;
		}
		i++;
	}
	if (!found)
	{
		return false;
	}
	value>>result;
	*field = result;
	return found;
}


bool ConfReader::success( )
{
	return read_success;
}

