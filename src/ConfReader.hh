#ifndef _CONFREADER_HH_
#define _CONFREADER_HH_
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

class ConfReader
{
 	string filename;
	string fields[16];
	string values[16];
	bool read_success;
	
	public:
	
	/**
	 * From a loaded configuration file, this function
	 * attempts to find the associated (int) value and if
	 * it is found, then it is stored in the variable
	 * pointed to by the pointer.
	 * @param _field: The field to find
	 * @param value: a pointer to the storage location
	 * @return: true if found, false if not found
	 * */
	bool field_int( string _field, int *value);

	/**
	 * From a loaded configuration file, this function
	 * attempts to find the associated (double) value and if
	 * it is found, then it is stored in the variable
	 * pointed to by the pointer.
	 * @param _field The field to find.
	 * @param value a pointer to the storage location
	 * @return true if the value is found, false otherwise
	 */
	bool field_double( string _field, double *value);

	/**
	 * From a loaded configuration file, this function
	 * attempts to find the associated (double) value and if
	 * it is found, then it is stored in the variable
	 * pointed to by the pointer.
	 * @param _field The field to find.
	 * @param value a pointer to the storage location
	 * @return true if the value is found, false otherwise
	 */
	bool field_string( string _field, string *value);

	ConfReader( string _filename );
	bool success();
};

#endif

