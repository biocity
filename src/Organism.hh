/* Copyright 2008 Jason Kim Chong Polak

Please respect the following:

This file is part of bioCity. bioCity is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _ORGANISM_HH_
#define _ORGANISM_HH_

class Organism
{
	//identification variables
	static int id;
	int ident;

	//state variables
	int type;
	int state;
	int food;

	double pos_x,next_x;
	double pos_y,next_y;
	int age;
	int next_offspring;
	
	//sensory variables
	int proximity;

	public:

	Organism( int _tp = 0,
			int state = 1,
			double _px = 0,
			double _py = 0
			);
	//--------------

	//Update To Next State
	void next_state( );

	//sensory functions
	double distance_to( const Organism _o );
	int get_proximity();
	void set_proximity( int _prox );
	void add_proximity( int _prox );

	//movement functions
	void change_pos(double, double);
	void add_pos( double, double );

	//state functions
	int get_age();
	void lifeup(); 	//deprecate
	void set_offspring( int _offspring );
	int get_offspring();
	int get_type();
	double get_x();
	double get_y();

	void eat_food();
	void use_food();
	int get_food();

	int get_state();
	int set_state( int );

	int identify();
	void info();
};
#endif
