/* Copyright 2008 Jason Kim Chong Polak

Please respect the following:

This file is part of bioCity. bioCity is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sstream>
#include <list>

#include "World.hh"
#include "ConfReader.hh"

#include <g2.h>
#include <g2_X11.h>
#include <g2_PS.h>
using namespace std;

int translate_data( string, string[], int );
void print_default( string );

int main()
{
	cout<<"#bioCity: Welcome to bioCity Matrix 0.4 (staging branch)"<<endl;
	cout<<"#bioCity: The current byte-size of the world class is "<<sizeof(World)<<endl;
	int k0;
	/*
	 * This part initializes environment variables
	 * and sets defaults if the configuration file
	 * cannot be read.
	 */
	//START Environment variables
	int psize = 4;
	double dim_x = 500, dim_y = 500;
	int time = 100;
	int graphics = 1;
	int scenario_num = 0;
	string scenario="scen_singl_pop";
	
	//array of scenario functions defined in World.cpp
	typedef void (World::*ptrUpdate)( );
	ptrUpdate update_func[][2] =
	{
		{ &World::scen_single_pop, &World::scen_single_pop_init}
	};
	string update_func_names[] =
	{
		"scen_single_pop"
	};
	int update_func_len = 1;

	//END environment variables
	
	ConfReader conf( "bioc.conf" );

	string currline;
	if ( !conf.success() )
	{
		cout<<"#Configuration file not found. Using defaults.";
	}
	else
	{
		if (!conf.field_int( "population", &psize) )
			print_default( "population" );
		if (!conf.field_double( "dimx", &dim_x) )
			print_default("dimx");
		if (!conf.field_double( "dimy", &dim_y) )
			print_default("dimy");
		if (!conf.field_int( "time", &time ) )
			print_default("time");
		if (!conf.field_int( "graphics", &graphics) )
			print_default("graphics");
		if (!conf.field_string( "scenario", &scenario) )
			print_default( "scenario" );

		int spacing = 21;
		cout<<"#bioCity: Current World Parameters"<<endl;
		cout<<setw(spacing)<<std::left<<"#Population Size:"<<std::right<<psize<<endl;
		cout<<setw(spacing)<<std::left<<"#Dimension X:"<<std::right<<dim_x<<endl;
		cout<<setw(spacing)<<std::left<<"#Dimension Y:"<<std::right<<dim_y<<endl;
		cout<<setw(spacing)<<std::left<<"#Time:"<<std::right<<time<<endl;
		cout<<setw(spacing)<<std::left<<"#Graphics:"<<std::right<<graphics<<endl;
		cout<<setw(spacing)<<std::left<<"#Scenario:"<<std::right<<scenario<<endl;
	}

	scenario_num = translate_data( scenario, update_func_names, update_func_len) - 1;

	/*
	 * This is where the magic happens :)
	 */
	int i = 0;
	World W1( psize, dim_x, dim_y);
	(W1.*update_func[scenario_num][1])();

	//Let the main know about all the functions
	//that can be use so that the user can
	//select them.

	int id,id0;
	if (graphics)
	{
		id = g2_open_vd();
		id0 = g2_open_X11( (int) dim_x , (int) dim_y  );
		//int id1 = g2_open_EPSF_CLIP( "locationd.eps", (int) dim_x, (int) dim_y );
		g2_attach(id, id0);
		//g2_attach(id, id1); 
		W1.print_graph( id );
	}
//	g2_detach(id, id1);
	cout<<"Timestep\tPopulation\t First\t Second"<<endl;
	int k;
	cout<<i<<"\t";
	W1.print_stats();
	for ( i = 0; i < time; i++ )
	{
		(W1.*update_func[scenario_num][0])();
		W1.move_next();
		if (i%20 == 0 && i > 0)
		{
			cout<<i<<"\t";
			W1.print_stats( );
			if (graphics) { W1.print_graph( id ); }
		}
		/* if (i == 500)
		{
			g2_attach( id, id1);
			W1.print_graph( id );
			g2_detach( id, id1);
		}*/
	}
	if (graphics) { W1.print_graph( id ); }
	cout<<i<<"\t";
	W1.print_stats( );
	//W1.complete_stats( );	
	cout<<"#The world has ended! Input anything to quit."<<endl;
	//cin>>k0;
	return 0;
}

int translate_data(string varName, string database[], int length)
{
	int numOptions = length;

	int loc = -1;
	for (int i = 0; i < numOptions; i++)
	{
		if (database[i] == varName)
		{
			loc = i+1;
		}
	}
	return loc;
}

void print_default(string field)
{
	cout<<"# WARNING: \""<<field<<"\" not found, using default!"<<endl;
}
