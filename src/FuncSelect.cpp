/* Copyright 2008 Jason Kim Chong Polak

Please respect the following:

This file is part of bioCity. bioCity is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "FuncSelect.hh"
#include <iostream>
using std::cout;
using std::endl;

FuncSelect::FuncSelect( )
{
	func_name = "default";
}

FuncSelect::FuncSelect( 
		string _func_name = "default",
		double *_params = NULL,
		int _params_len = 2,
		gsl_rng *_r = NULL
		)
{
	func_name = _func_name;
	params_len = _params_len;
	params = new double[params_len];
	rgen = _r; //we don't want to deep copy!!!

	for (int i = 0; i < params_len; i++)
	{
		params[i] = _params[i];
	}
}

FuncSelect::~FuncSelect( )
{
	if (params != NULL)
	{
		delete[] params;
	}
}

double FuncSelect::evaluate()
{
	double result;

	if ( func_name == "default" )
	{
		result = -1;
		cout<<"#Warning! Default selected...."<<endl;
	} //Random Number Distributions
	else if ( func_name == "gsl_cdf_gaussian_P" )
	{
		double quantile = params[0];
		double mean = params[1];
		double var = params[2];
		result = gsl_cdf_gaussian_P( quantile - mean, var ); 
	}
	else if (func_name == "gsl_cdf_exponential_P" )
	{
		double quantile = params[0];
		double mu = params[1];	// here f(x) = 1/mue^(-x/mu)
		result = gsl_cdf_exponential_P( quantile, mu );
	}
	else if ( func_name == "gsl_cdf_gamma_P" )
	{
		double quantile = params[0];
		double a = params[1];
		double b = params[2];
		result = gsl_cdf_gamma_P( quantile, a, b );
	}
	else if ( func_name == "gsl_cdf_tdist_P" )
	{
		double quantile = params[0];
		double nu = params[1];
		result = gsl_cdf_tdist_P( quantile, nu );
	}
	else if ( func_name == "yousuck")
	{
		result = -999;
	}
	else
	{
		cout<<"#Warning! Some function was specified that is not in the database."<<endl;
		result = 0;
	}

	return result;
}
